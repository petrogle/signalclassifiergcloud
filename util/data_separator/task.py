import logging
import os
import re
import sys
from datetime import datetime

from .separator import CategorisedSeparator
from ..fs_util.samples_loader import SamplesLoader

logging.basicConfig(stream=sys.stderr, level=logging.ERROR)

separator = CategorisedSeparator()
loader = SamplesLoader()

input_dir = sys.argv[1]
output_dir = sys.argv[2]
ratio = float(sys.argv[3])

train_values = {}
test_values = {}
i = 0
values = loader.load_from_dir(input_dir, re.compile('.+header.csv$'))
max_value = 0
for event_name, list_of_lists_of_values in values.items():
    max_value = max(max_value, max(max(list_of_values) for list_of_values in list_of_lists_of_values))
    logging.info('Separating category %d/%d...', i, len(values.keys()))
    train_values[event_name], test_values[event_name] = separator.separate(ratio, list_of_lists_of_values)
    i += 1
print('Max value: %d' % max_value)

save_root = output_dir.rstrip('/') + '/' + datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
os.makedirs(save_root)

logging.info('Saving training data...')
train_save_root = save_root + '/train'
os.makedirs(train_save_root)
loader.save_dir(train_values, train_save_root)

logging.info('Saving testing data...')
test_save_root = save_root + '/test'
os.makedirs(test_save_root)
loader.save_dir(test_values, test_save_root)
