from abc import ABC
from typing import Dict, Sequence, Any

from util.input_transformers import BaseInputTransformer


class BaseSingleSequenceBasedTransformer(BaseInputTransformer, ABC):

    def transform(self, seqs_of_seqs_by_class_id: Dict[int, Sequence[Sequence[Any]]]) -> Dict[int, Sequence[Sequence[Any]]]:
        return {
            class_id: [self.transform_single_sequence(sequence) for sequence in sequences] for
            class_id, sequences in seqs_of_seqs_by_class_id.items()
        }
