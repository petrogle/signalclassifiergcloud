#!/usr/bin/env bash

echo "8 classes"
for attempt in {6..10}; do

    echo "Attempt #${attempt}:"

    venv/bin/python -m trainer.task \
        --job-dir /home/gleb/PycharmProjects/SignalClassificator/EventProcessing/out \
        --model-path /home/gleb/Documents/diplomka/models/model.hdf5 \
        --format hdf5 \
        --epochs 5 \
        --map 0.0 1.1 2.2 3.3 4.4 5.5 6.6 7.7


    venv/bin/python -m predictor.task \
        --job-dir /home/gleb/PycharmProjects/SignalClassificator/EventProcessing/out \
        --model-path /home/gleb/Documents/diplomka/models/model.hdf5 \
        --format hdf5 \
        --map 0.0 1.1 2.2 3.3 4.4 5.5 6.6 7.7

done

echo "7 classes"
for attempt in {1..10}; do

    echo "Attempt #${attempt}:"

    venv/bin/python -m trainer.task \
        --job-dir /home/gleb/PycharmProjects/SignalClassificator/EventProcessing/out \
        --model-path /home/gleb/Documents/diplomka/models/model.hdf5 \
        --format hdf5 \
        --epochs 5 \
        --map 0.0 1.1 2.1 3.2 4.3 5.4 6.5 7.6


    venv/bin/python -m predictor.task \
        --job-dir /home/gleb/PycharmProjects/SignalClassificator/EventProcessing/out \
        --model-path /home/gleb/Documents/diplomka/models/model.hdf5 \
        --format hdf5 \
        --map 0.0 1.1 2.1 3.2 4.3 5.4 6.5 7.6

done

echo "6 classes"
for attempt in {1..10}; do

    echo "Attempt #${attempt}:"

    venv/bin/python -m trainer.task \
        --job-dir /home/gleb/PycharmProjects/SignalClassificator/EventProcessing/out \
        --model-path /home/gleb/Documents/diplomka/models/model.hdf5 \
        --format hdf5 \
        --epochs 5 \
        --map 0.0 1.1 2.1 3.1 4.2 5.3 6.4 7.5


    venv/bin/python -m predictor.task \
        --job-dir /home/gleb/PycharmProjects/SignalClassificator/EventProcessing/out \
        --model-path /home/gleb/Documents/diplomka/models/model.hdf5 \
        --format hdf5 \
        --map 0.0 1.1 2.1 3.1 4.2 5.3 6.4 7.5

done