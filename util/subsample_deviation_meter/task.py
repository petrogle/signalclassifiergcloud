import logging
import re
import sys

from util.fs_util.samples_loader import SamplesLoader
from util.subsample_deviation_meter.subsample_deviation_meter import SubsampleDeviationMeter

FACTORS = range(1, 10)

logging.basicConfig(stream=sys.stderr, level=logging.INFO)
loader = SamplesLoader()
meter = SubsampleDeviationMeter(FACTORS)

max_deviations = {f: 0 for f in FACTORS}
samples = loader.load_from_dir(sys.argv[1], re.compile('.+_Samples.csv$'))

total_items_count = sum(len(v) for k, v in samples.items())
i = 0
for key, list_of_lists_of_values in samples.items():
    for list_of_values in list_of_lists_of_values:
        logging.info('Processing %d of %d', i, total_items_count)
        i += 1

        relative_deviation = meter.calculate_relative_deviation(list_of_values)
        max_deviations = {f: max(max_deviations[f], d) for f, d in relative_deviation.items()}

for f, d in max_deviations.items():
    print('Max deviation for factor %d is %f' % (f, d))
