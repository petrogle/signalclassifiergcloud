#!/usr/bin/env bash

MODEL_NAME=$1
BUCKET=$2

MODEL_URI="${BUCKET}${3}"
PACKAGE_URI="${BUCKET}${4}"
VERSION_NAME="v$(date +"%Y%m%d_%H%M%S")"
OUTPUT_PATH=${BUCKET}/deployed_models/${MODEL_NAME}
REGION='eu-west1'

echo "Creating version with following parameters:"
echo "MODEL_NAME = ${MODEL_NAME}"
echo "MODEL_URI = ${MODEL_URI}"
echo "PACKAGE_URI = ${PACKAGE_URI}"
echo "OUTPUT_PATH = ${OUTPUT_PATH}"
echo "VERSION_NAME = ${VERSION_NAME}"

echo "Checking model..."
if gcloud ai-platform models describe $MODEL_NAME 1>/dev/null 2>/dev/null; then
  echo "Model already exists"
else
  echo "Creating model..."
  gcloud ai-platform models create $MODEL_NAME
fi

echo "Getting full model binaries path..."
MODEL_BINARIES=$(gsutil ls ${MODEL_URI} | tail -1)
echo "Model binaries path is ${MODEL_BINARIES}"

gcloud beta ai-platform versions create "${VERSION_NAME}" \
    --model "$MODEL_NAME" \
    --origin "$MODEL_BINARIES" \
    --runtime-version 1.13 \
    --python-version 3.5 \
    --prediction-class "predictor.SignalClassifierPredictor" \
    --package-uris "$PACKAGE_URI"
