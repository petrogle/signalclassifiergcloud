#!/bin/bash

DATA_DIR=$1
DATA_ROOT=$(dirname ${DATA_DIR})
DATA_DIR_NAME=$(basename ${DATA_DIR})
EXPERIMENT_DIR="${DATA_ROOT}/${DATA_DIR_NAME}_exp_$(date +%Y-%m-%d_%H:%M:%S)"
PROJECT_ROOT="/home/gleb/Documents/SignalClassifierGCloud"
RATIO=4

echo "Separating..."
MAX_VALUE=$(
    $PROJECT_ROOT/venv/bin/python -m util.data_separator.task "${DATA_DIR}" "${EXPERIMENT_DIR}" $RATIO \
    | grep -oP '(?<=Max value: )\d+'
)
TRAIN_DIR=`find "${EXPERIMENT_DIR}" -type d -name train`
TEST_DIR=`find "${EXPERIMENT_DIR}" -type d -name test`

echo "Training CNN..."
TRAIN_OUTPUT=$(
    $PROJECT_ROOT/venv/bin/python -m trainer.task --job-dir "${TRAIN_DIR}" \
        --model-path "${EXPERIMENT_DIR}/saved_model_cnn" \
        --format saved_model \
        --model-type cnn \
        --epochs 10 \
        --max-possible-value $MAX_VALUE \
        | tee /dev/tty
)
INPUT_LENGTH=$(echo $TRAIN_OUTPUT | grep -oP '(?<=Padding length set to )\d+')

echo "Predicting CNN..."
$PROJECT_ROOT/venv/bin/python -m predictor.task --job-dir "${TEST_DIR}" \
    --model-path "${EXPERIMENT_DIR}/saved_model_cnn" \
    --format saved_model \
    --input-length $INPUT_LENGTH 2>/dev/null

echo "Training dense..."
TRAIN_OUTPUT=$(
    $PROJECT_ROOT/venv/bin/python -m trainer.task --job-dir "${TRAIN_DIR}" \
        --model-path "${EXPERIMENT_DIR}/saved_model_dense" \
        --format saved_model \
        --model-type dense \
        --epochs 10 \
        --max-possible-value $MAX_VALUE \
        | tee /dev/tty
)
INPUT_LENGTH=$(echo $TRAIN_OUTPUT | grep -oP '(?<=Padding length set to )\d+')

echo "Predicting dense..."
$PROJECT_ROOT/venv/bin/python -m predictor.task --job-dir "${TEST_DIR}" \
    --model-path "${EXPERIMENT_DIR}/saved_model_dense" \
    --format saved_model \
    --input-length $INPUT_LENGTH 2>/dev/null
