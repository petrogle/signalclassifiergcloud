from setuptools import setup

setup(
    name='SignalClassifiePredictor',
    packages=['predictor', 'util'],
    author='Gleb Petrovichev',
    author_email='petrogle@fel.cvut.cz',
    install_requires=['numpy', 'tensorflow', 'scipy', 'google-cloud-storage', 'scikit-learn', 'matplotlib', 'dependency-injector',
                      'psutil']
)
