#!/usr/bin/env bash

NOW=$(date +"%Y%m%d_%H%M%S")

BUCKET=$1
PREDICTOR_DEPLOYMENT_PATH="${BUCKET}/predictor/${NOW}/signal_classifier_predictor-0.0.0.tar.gz"

python setup_predictor.py sdist --formats=gztar
gsutil cp ./dist/SignalClassifiePredictor-0.0.0.tar.gz ${PREDICTOR_DEPLOYMENT_PATH}

echo "Package URI: ${PREDICTOR_DEPLOYMENT_PATH}"
