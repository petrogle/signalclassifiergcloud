import argparse
from typing import List

from util.fs_util.model_export_format_enum import ModelExportFormatEnum
from util.model_type_enum import ModelTypeEnum


class BaseArgumentParser(argparse.ArgumentParser):

    def __init__(self):
        super().__init__()
        self.setup()

    def setup(self):
        self.add_argument("--job-dir", type=str, help="Path to the directory containing samples files")
        self.add_argument("--model-path", type=str, help="""Path to the model. File in case of HDF5 format or directory,
            when Saved model format is selected (see --model-type option)"""
                          )
        self.add_argument("--format", type=str, nargs="?", default="hdf5",
                          choices=list(map(lambda f: f.name, ModelExportFormatEnum)), help="""Model format used to
                            save model (trainer) or to load it (predictor)""")
        self.add_argument("--map", type=str, nargs="*", default=None, help="""Instructions for a Remapping input
            transformer in a following format: x[1].y[1] x[2].y[2] ... x[N], y[N]. Where x[i] is an integer representing
            a class from which samples are remapped to a class defined by y[i].""")
        self.add_argument("--fft", action="store_true", help="""When present, enables FFT input transformer""")
        self.add_argument("--input-length", type=int, default=None, help="""This option is used to set a fixed length
            of sequences in case, when it is not detectable otherwise (mainly in trainer mode)""")
        self.add_argument("--model-type", type=str, nargs="?", default="cnn",
                          choices=list(map(lambda f: f.name, ModelTypeEnum)), help="""Neural network design used to
                                implement a model""")
        self.add_argument("--log-dir", type=str, nargs="?", default=None, help="""Path to the directory, where logs are
            to be stored. Also enables additional logging""")

    def parse_args(self, args: List[str] = None, namespace: argparse.Namespace = None):
        parsed_args = super().parse_args(args, namespace)
        if parsed_args.map is not None:
            parsed_args.map = {
                int(ip): int(fp) for ip, fp in [f.split(".") for f in parsed_args.map]
            }
        return parsed_args
