from abc import ABC, abstractmethod
from tempfile import NamedTemporaryFile, _TemporaryFileWrapper
from typing import TextIO, Callable

from google.cloud.storage import Client, Bucket
from tensorflow.contrib.saved_model import save_keras_model
from tensorflow.python.keras.models import save_model, Model

from util.fs_util.functions import parse_google_cloud_storage_address, is_google_cloud_storage_address, \
    get_google_cloud_storage_client
from util.fs_util.model_export_format_enum import ModelExportFormatEnum


class BaseModelFileWriter(ABC):
    save_model_callback = ...  # type: Callable[[Model, str], None]
    file_name = ...  # type: str

    def __init__(self, file_name: str, save_model_callback: Callable[[Model, str], None]):
        self.save_model_callback = save_model_callback
        self.file_name = file_name

    @abstractmethod
    def write(self, model: Model) -> None:
        pass

    @staticmethod
    def get_instance(
            file_name: str,
            format: ModelExportFormatEnum = ModelExportFormatEnum.hdf5
    ) -> 'BaseModelFileWriter':
        if format == ModelExportFormatEnum.hdf5:
            save_model_callback = lambda model, filepath: save_model(model, filepath, include_optimizer=False)
        else:
            return SavedModelFileWriter(file_name)

        if is_google_cloud_storage_address(file_name):
            bucket_name, file_name = parse_google_cloud_storage_address(file_name)
            return GoogleCloudStorageModelFileWriter(
                get_google_cloud_storage_client(),
                bucket_name,
                file_name,
                save_model_callback
            )
        else:
            return LocalModelFileWriter(file_name, save_model_callback)


class LocalModelFileWriter(BaseModelFileWriter):

    def write(self, model: Model) -> None:
        self.save_model_callback(model, self.file_name)


class GoogleCloudStorageModelFileWriter(BaseModelFileWriter):
    bucket = ...  # type: Bucket

    def __init__(
            self,
            client: Client,
            bucket_name: str,
            file_name: str,
            save_model_callback: Callable[[Model, str], None]
    ):
        super().__init__(file_name, save_model_callback)
        self.bucket = client.bucket(bucket_name)

    def write(self, model: Model) -> None:
        temporary_file = NamedTemporaryFile()  # type: Union[_TemporaryFileWrapper, TextIO]
        self.save_model_callback(model, temporary_file.name)
        temporary_file.flush()
        self.upload_local_file(temporary_file.name, target_path=self.file_name)
        temporary_file.close()

    def upload_local_file(self, source_path: str, target_path: str) -> None:
        blob = self.bucket.blob(target_path)
        blob.upload_from_filename(source_path)


class SavedModelFileWriter(BaseModelFileWriter):
    """
    https://cloud.google.com/ml-engine/docs/tensorflow/getting-started-keras
    """

    def __init__(self, file_name: str):
        super().__init__(file_name, lambda: None)

    def write(self, model: Model) -> None:
        save_keras_model(model, self.file_name)
