from os import path

GOOGLE_CLOUD_STORAGE_PREFIX = "gs://"
GOOGLE_CREDENTIALS_PATH = path.realpath(path.dirname(__file__)) + "/gcs_credentials.json"
