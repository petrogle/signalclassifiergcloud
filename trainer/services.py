from dependency_injector import containers, providers
from trainer.model_builder import ModelFactory
from trainer.model_runner import ModelRunner
from trainer.task_class import TaskClass
from trainer.trainer_argument_parser import TrainerArgumentParser
from util.common_services import common_services


class PrimaryServices(containers.DeclarativeContainer):
    argument_parser = providers.Singleton(TrainerArgumentParser)
    model_factory = providers.Singleton(ModelFactory)


class SecondaryServices(PrimaryServices):
    model_runner = providers.Factory(ModelRunner, PrimaryServices.argument_parser())


class Services(SecondaryServices):
    task_class = providers.Factory(TaskClass, argument_parser=SecondaryServices.argument_parser(),
                                   samples_loader=common_services.samples_loader(),
                                   model_factory=SecondaryServices.model_factory(),
                                   model_runner=SecondaryServices.model_runner())
