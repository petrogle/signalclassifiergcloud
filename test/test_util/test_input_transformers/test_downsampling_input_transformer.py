from unittest import TestCase
from numpy.testing import assert_almost_equal
import numpy as np

from util.input_transformers import DownsamplingInputTransformer


class TestDownsamplingInputTransformer(TestCase):
    def test_transform_single_sequence_positive_length(self):
        transformer = DownsamplingInputTransformer(2)
        self.assertEqual(len(transformer.transform_single_sequence(range(14))), 7)

    def test_transform_single_sequence_positive_form(self):
        transformer = DownsamplingInputTransformer(2)
        assert_almost_equal(transformer.transform_single_sequence(range(14)), range(0, 14, 2), 3)

    def test_transform_single_sequence_positive_no_negatives(self):
        transformer = DownsamplingInputTransformer(2)
        self.assertFalse(
            (np.asarray(transformer.transform_single_sequence(np.sin(np.arange(0, 2 * np.pi, 0.1)))) < 0).any()
        )

    def test_transform_single_sequence_negative_too_short(self):
        transformer = DownsamplingInputTransformer(2)
        self.assertRaises(ValueError, transformer.transform_single_sequence, [1, 2, 3])

    def test_transform_single_sequence_negative_bad_downsampling_factor(self):
        transformer = DownsamplingInputTransformer(0)
        self.assertRaises(ZeroDivisionError, transformer.transform_single_sequence, [1, 2, 3])
        transformer = DownsamplingInputTransformer(-1)
        self.assertRaises(ValueError, transformer.transform_single_sequence, [1, 2, 3])
