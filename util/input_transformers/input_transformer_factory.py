from typing import Optional, Dict, List

from util.input_transformers.input_transformers_container import InputTransformersContainer
from util.model_type_enum import ModelTypeEnum
from . import *


class InputTransformerFactory:
    _container = ...  # type: InputTransformersContainer

    _model_type = ...  # type: Optional[ModelTypeEnum]
    _enable_padding = ...  # type: bool
    _class_mapping = ...  # type: Optional[Dict[int, int]]
    _enable_fft = ...  # type: bool
    _required_input_length = ...  # type: Optional[int]
    _force_downsample_factor = ...  # type: Optional[int]
    _max_possible_value = ...  # type: Optional[int]

    def __init__(
            self,
            model_type: Optional[ModelTypeEnum] = None,
            enable_padding: bool = False,
            class_mapping: Optional[Dict[int, int]] = None,
            enable_fft: bool = False,
            required_input_length: Optional[int] = None,
            force_downsample_factor: Optional[int] = None,
            max_possible_value: Optional[int] = None,
    ):
        self._model_type = model_type
        self._enable_padding = enable_padding
        self._class_mapping = class_mapping
        self._enable_fft = enable_fft
        self._required_input_length = required_input_length
        self._force_downsample_factor = force_downsample_factor
        self._max_possible_value = max_possible_value
        self._container = InputTransformersContainer(config=self._get_container_config())

    def get_transformers_chain(self) -> List[BaseInputTransformer]:
        chain = []
        if self._required_input_length is not None or (self._force_downsample_factor is not None and self._force_downsample_factor != 1):
            chain.append(self._container.downsampler())
        if self._enable_fft:
            chain.append(self._container.fft())
        if self._model_type == ModelTypeEnum.dense:
            if self._max_possible_value is None:
                raise ValueError('max_possible_value must be specified for dense network model')
            chain.append(self._container.normalizing())
        if self._enable_padding:
            chain.append(self._container.padding())
        if self._class_mapping is not None:
            chain.append(self._container.events_remapping())
        return chain

    def _get_container_config(self) -> Dict:
        return {
            'required_input_length': self._required_input_length,
            'class_mapping': self._class_mapping,
            'max_possible_value': self._max_possible_value,
            'force_resample_factor': self._force_downsample_factor,
        }
