from typing import List, Union

import tensorflow as tf
from tensorflow.python.keras import Model

from trainer.trainer_argument_parser import TrainerArgumentParser
from util.named_tuples import EventsLabels


class ModelRunner:
    _log_dir = ...  # type: Union[str, None]

    def __init__(self, argument_parser: TrainerArgumentParser):
        self._log_dir = argument_parser.parse_args().log_dir

    def fit(self, model: Model, events_labels: EventsLabels, epochs: int) -> None:
        model.fit(
            events_labels.events,
            events_labels.labels,
            epochs=epochs,
            callbacks=self._get_fit_callbacks(),
        )

    def _get_fit_callbacks(self) -> List[tf.keras.callbacks.Callback]:
        if self._log_dir is None:
            return []
        print('Log dir: ' + self._log_dir)
        return [
            tf.keras.callbacks.TensorBoard(
                log_dir=self._log_dir,
                histogram_freq=0,
                write_graph=True,
                embeddings_freq=0
            ),
        ]
