from typing import Sequence, Any

from scipy.fftpack import fft

from util.input_transformers.base_single_sequence_based_transformer import BaseSingleSequenceBasedTransformer


class FftTransformer(BaseSingleSequenceBasedTransformer):

    def transform_single_sequence(self, sequence: Sequence[Any]) -> Sequence[Any]:
        return fft(sequence)
