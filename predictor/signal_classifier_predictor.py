import logging
import time
from typing import List, Sequence, Dict, Optional

import numpy as np
import tensorflow as tf
from tensorflow.keras.callbacks import Callback

from predictor.base_predictor import BasePredictor
from util.fs_util.model_file_reader import BaseModelFileReader
from util.input_transformers.input_transformer_factory import InputTransformerFactory, ThresholdDownsamplerTransformer
from util.model_type_enum import ModelTypeEnum


class SignalClassifierPredictor(BasePredictor):
    _predictor_callbacks = ...  # type: List[Callback]
    _transformer_factory = ...  # type: InputTransformerFactory
    _model = ...  # type: tf.keras.models.Model

    def __init__(self, model: tf.keras.models.Model):
        self._model = model

    def predict(self, instances: List[List[int]], **kwargs) -> List:
        self.setup(kwargs, self._get_input_length(instances))

        return self.do_predict(instances)

    def predict_single_instance(self, instance: List[int]) -> int:
        return self.do_predict([instance])[0]

    def do_predict(self, instances: List[List[int]]):
        logging.info('%f\tTransforming...', time.time())
        instances = self._transform_instances(instances)
        logging.info('%f\tPredicting...', time.time())
        outputs = self._model.predict(np.asarray(instances), callbacks=self._predictor_callbacks)
        return list(np.argmax(outputs, axis=1))

    @classmethod
    def from_path(cls, model_dir: str) -> BasePredictor:
        return cls(BaseModelFileReader.get_instance_by_file_name(model_dir).read())

    def setup(self, kwargs: Dict, required_input_length: Optional[int] = None):
        input_length = required_input_length if required_input_length is not None else self._get_input_length()
        self._transformer_factory = InputTransformerFactory(
            model_type=ModelTypeEnum[kwargs['model_type']],
            enable_padding=True,
            class_mapping=kwargs['map'],
            enable_fft=kwargs['fft'],
            required_input_length=input_length,
            force_downsample_factor=kwargs['resample_factor']
        )
        self._predictor_callbacks = self._setup_callbacks(kwargs['log_dir'])

    def _transform_instances(self, instances: List[List[int]]) -> Sequence[Sequence[int]]:
        for transformer in self._transformer_factory.get_transformers_chain():
            logging.debug("Applying filter " + transformer.__class__.__name__)
            if isinstance(transformer, ThresholdDownsamplerTransformer):
                transformer.set_resample_factor_by_max_sample_length(max(len(event_values) for event_values in instances))
            instances = [transformer.transform_single_sequence(instance) for instance in instances]

        return instances

    @staticmethod
    def _setup_callbacks(log_dir: str) -> List[tf.keras.callbacks.Callback]:
        if log_dir is None:
            logging.info('Saving TensorBoard logs is disabled')
            return []
        else:
            logging.info('TensorBoard log dir is set up to %s', log_dir)
            return [
                tf.keras.callbacks.TensorBoard(
                    log_dir=log_dir,
                    histogram_freq=0,
                    write_graph=True,
                    embeddings_freq=0
                ),
            ]

    def _get_input_length(self, sequences: Optional[List[List]] = None):
        input_length = None

        try:
            input_length = self._model.input_shape[1]
        except AttributeError:
            pass

        try:
            input_length = self._model.input_spec.axes[-1]
        except AttributeError:
            pass

        if input_length is None:
            if sequences is not None:
                input_length = max(len(sequence) for sequence in sequences)
            else:
                raise RuntimeError('Input length cannot be determined by model, sequence to estimate is not provided')

        return input_length
