import math
import random
from typing import List, Tuple

from util.fs_util.fs_traverser import BaseFSTraverser


class CategorisedSeparator:

    def separate(self, ratio: float, data: List) -> Tuple[List, List]:
        lesser_part = min(1 / (ratio + 1), 1 - 1 / (ratio + 1))
        abs_lesser_part = math.floor(len(data) * lesser_part)
        abs_greater_part = len(data) - abs_lesser_part
        random.shuffle(data)
        return data[-abs_greater_part:], data[:abs_lesser_part]

    def sub_separate(self, ratio: float, data: List[List]) -> Tuple[List, List]:
        greater_part_list = []
        lesser_part_list = []
        for sublist in data:
            greater_part_sublist, lesser_part_sublist = self.separate(ratio, sublist)
            lesser_part_list.append(lesser_part_sublist)
            greater_part_list.append(greater_part_sublist)
        return greater_part_list, lesser_part_list


class FilesInDirsSeparator(CategorisedSeparator):
    _fs_traverser = ...  # type: BaseFSTraverser

    def __init__(self, fs_traverser: BaseFSTraverser):
        self._fs_traverser = fs_traverser

    def separate_files(self, ratio: float) -> Tuple[List[List[str]], List[List[str]]]:
        return super().sub_separate(ratio, [x for k, x in self._fs_traverser.get_files_by_event_id().items()])
