import io
import unittest
from io import StringIO

from util.fs_util.samples_loader.samples_loader import SamplesLoader


class TestSamplesLoader(unittest.TestCase):
    INPUT_TEST_FILE_CONTENTS = (
        "rIndex, ID, timeStr, timeOff, rawValue, diffValue, trendLevel\n"
        " 945699, 0, 18-04-2019 08:54:08.145500, 60517089, 1496468.0, 120.0, -1250.0\n"
        " 945700, 0, 18-04-2019 08:54:08.145564, 60517153, 1496708.0, 240.0, -61410.0\n"
        " 945701, 0, 18-04-2019 08:54:08.145628, 60517217, 1498138.0, 1430.0, -61410.0\n"
        " 945702, 0, 18-04-2019 08:54:08.145692, 60517281, 1505918.0, 7780.0, -61410.0\n"
        " 945703, 0, 18-04-2019 08:54:08.145756, 60517345, 1513878.0, 7960.0, -61410.0\n"
        " 149319, 1, 15-04-2019 02:56:00.332135, 9555219, 2198098.0, 3870.0, -2620.0\n"
        " 149320, 1, 15-04-2019 02:56:00.332199, 9555283, 2202438.0, 4340.0, -4000.0\n"
        " 149321, 1, 15-04-2019 02:56:00.332263, 9555347, 2203218.0, 780.0, -4000.0\n"
        " 149322, 1, 15-04-2019 02:56:00.332327, 9555411, 2201638.0, -1580.0, -4000.0\n"
        " 149323, 1, 15-04-2019 02:56:00.332391, 9555475, 2200168.0, -1470.0, -4000.0\n"
        " 149324, 1, 15-04-2019 02:56:00.332455, 9555539, 2203358.0, 3190.0, -4000.0\n"
        " 149325, 1, 15-04-2019 02:56:00.332519, 9555603, 2206728.0, 3370.0, -4000.0\n"
        " 149326, 1, 15-04-2019 02:56:00.332584, 9555668, 2207568.0, 840.0, -4000.0\n"
        " 149327, 1, 15-04-2019 02:56:00.332648, 9555732, 2208248.0, 680.0, -4000.0"
    )

    OUTPUT_TEST_FILE_CONTENTS = (
        "ID,rawValue\n"
        "0,1496468.0\n"
        "0,1496708.0\n"
        "0,1498138.0\n"
        "0,1505918.0\n"
        "0,1513878.0\n"
        "1,2198098.0\n"
        "1,2202438.0\n"
        "1,2203218.0\n"
        "1,2201638.0\n"
        "1,2200168.0\n"
        "1,2203358.0\n"
        "1,2206728.0\n"
        "1,2207568.0\n"
        "1,2208248.0"
    )

    EXPECTED_LIST_OF_LIST_OF_VALUES = [
        [1496468, 1496708, 1498138, 1505918, 1513878],
        [2198098, 2202438, 2203218, 2201638, 2200168, 2203358, 2206728, 2207568, 2208248],
    ]

    samples_loader = ...  # type: SamplesLoader
    simulated_file = ...  # type: StringIO

    def setUp(self) -> None:
        self.samples_loader = SamplesLoader()
        self.simulated_file = io.StringIO()

    def test_load_single_file(self):
        self.simulated_file.write(self.INPUT_TEST_FILE_CONTENTS)
        self.simulated_file.seek(0)
        list_of_list_of_values = self.samples_loader.load_single_file(self.simulated_file)
        self.assertEqual(self.EXPECTED_LIST_OF_LIST_OF_VALUES, list_of_list_of_values)

    def test_save_single_file(self):
        self.samples_loader.save_single_file(self.EXPECTED_LIST_OF_LIST_OF_VALUES, self.simulated_file)
        self.assertEqual(self.OUTPUT_TEST_FILE_CONTENTS, self.simulated_file.getvalue().strip())


if __name__ == '__main__':
    unittest.main()
