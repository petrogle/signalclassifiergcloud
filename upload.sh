#!/usr/bin/env bash

# Google Cloud parameters
BUCKET="gs://signal-classifier-samples"
TRAINING_DATA_PATH="/2020-01-06_14:19:20/train"

# Training parameters
INPUT_LENGTH=25951
MODEL_TYPE="cnn"

NOW=$(date +"%Y%m%d_%H%M%S")
TRAINING_MODULE="trainer"
JOB_NAME="trainer_${NOW}"
TRAINING_DATA_DIR="${BUCKET}${TRAINING_DATA_PATH}"
OUTPUT_DIR="$BUCKET/output"
JOB_DIR="$BUCKET/$JOB_NAME"
LOG_DIR="$BUCKET/logs"
PACKAGE_STAGING_PATH="$BUCKET/trainer_$NOW"

gcloud ai-platform jobs submit training ${JOB_NAME} \
    --staging-bucket ${BUCKET} \
    --job-dir ${TRAINING_DATA_DIR}  \
    --package-path "trainer" \
    --module-name ${TRAINING_MODULE} \
    --runtime-version 1.13 \
    --python-version 3.5 \
    --config config.yaml \
    -- \
    --model-path "$OUTPUT_DIR/model_$NOW/" \
    --format saved_model \
    --epochs 10 \
    --model-type "${MODEL_TYPE}" \
    --log-dir "${LOG_DIR}\run_${NOW}" \
    --input-length $INPUT_LENGTH

gcloud ai-platform jobs stream-logs ${JOB_NAME}
