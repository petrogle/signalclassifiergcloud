import os
from abc import ABC, abstractmethod
from typing import Dict, Optional, List


from google.cloud.storage import Client, Bucket

from util.fs_util.functions import parse_google_cloud_storage_address, is_google_cloud_storage_address, \
    get_google_cloud_storage_client, get_google_storage_fqn


class BaseFSTraverser(ABC):
    directory = ...  # type: str
    files_by_event_id = None  # type: Optional[Dict[int, List[str]]]

    def __init__(self, directory: str):
        self.directory = directory

    def get_files_by_event_id(self, max_count: int = 0) -> Dict[int, List[str]]:
        if self.files_by_event_id is None:
            self.files_by_event_id = {}
            self._fill_files_by_event_id(max_count)
        return self.files_by_event_id

    @staticmethod
    def get_instance_by_dir(directory: str) -> 'BaseFSTraverser':
        if is_google_cloud_storage_address(directory):
            bucket_name, directory = parse_google_cloud_storage_address(directory)
            return GoogleCloudStorageFSTraverser(get_google_cloud_storage_client(), bucket_name, directory)
        else:
            return LocalFSTraverser(directory)

    @abstractmethod
    def _fill_files_by_event_id(self, max_count: int) -> None:
        pass

    def _process_file_name(self, file_name: str, max_count: int) -> None:
        if file_name.endswith('.csv'):
            event_id = int(os.path.splitext(file_name)[0].split('_')[-1])
            if event_id not in self.files_by_event_id:
                self.files_by_event_id[event_id] = []
            if max_count == 0 or len(self.files_by_event_id[event_id]) < max_count:
                self.files_by_event_id[event_id].append(file_name)


class LocalFSTraverser(BaseFSTraverser):

    def _fill_files_by_event_id(self, max_count: int) -> None:
        for p, d, f in os.walk(self.directory):
            for file in f:
                self._process_file_name(p + '/' + file, max_count)


class GoogleCloudStorageFSTraverser(BaseFSTraverser):
    bucket = ...  # type: Bucket

    def __init__(self, client: Client, bucket_name: str, directory: str):
        super().__init__(directory)
        self.bucket = client.bucket(bucket_name)

    def _fill_files_by_event_id(self, max_count: int) -> None:
        for blob in self.bucket.list_blobs(prefix='training_data/'):
            self._process_file_name(get_google_storage_fqn(self.bucket, blob), max_count)
