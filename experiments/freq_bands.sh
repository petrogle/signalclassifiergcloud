#!/bin/bash

DATA_DIR=$1
DATA_ROOT=$(dirname ${DATA_DIR})
DATA_DIR_NAME=$(basename ${DATA_DIR})

EXPERIMENT_NAME="2019-11-05_exp_2019-11-13_13:31:16"

#EXPERIMENT_NAME="${DATA_DIR_NAME}_exp_$(date +%Y-%m-%d_%H:%M:%S)"
EXPERIMENT_DIR="${DATA_ROOT}/${EXPERIMENT_NAME}"
PROJECT_ROOT="/home/gleb/Documents/SignalClassifierGCloud"
RATIO=4

BAND_NAME="0-7800"

#ls -v1 $DATA_DIR | while read BAND_NAME; do
    echo "Separating ${BAND_NAME}..."
    MAX_VALUE=$(
        $PROJECT_ROOT/venv/bin/python -m util.data_separator.task "${DATA_DIR}/${BAND_NAME}" "${EXPERIMENT_DIR}/${BAND_NAME}" $RATIO \
        | grep -oP '(?<=Max value: )\d+'
    )
    TRAIN_DIR=`find "${EXPERIMENT_DIR}/${BAND_NAME}" -type d -name train`
    TEST_DIR=`find "${EXPERIMENT_DIR}/${BAND_NAME}" -type d -name test`
    echo "Max value: ${MAX_VALUE}"

    echo "Training ${BAND_NAME}..."
    TRAIN_OUTPUT=$(
        $PROJECT_ROOT/venv/bin/python -m trainer.task --job-dir "${TRAIN_DIR}" \
            --model-path "${EXPERIMENT_DIR}/${BAND_NAME}/saved_model" \
            --format saved_model \
            --model-type cnn \
            --epochs 10 \
            --max-possible-value $MAX_VALUE \
            --log-dir "/home/gleb/Documents/SignalClassifierGCloud/logs/${EXPERIMENT_NAME}_${BAND_NAME}_fit" \
            2>&1 \
            | grep -v 'FutureWarning' \
            | tee /dev/tty
    )
    INPUT_LENGTH=$(echo $TRAIN_OUTPUT | grep -oP '(?<=Padding length set to )\d+')

    echo "Predicting ${BAND_NAME}..."
    $PROJECT_ROOT/venv/bin/python -m predictor.task --job-dir "${TEST_DIR}" \
        --model-path "${EXPERIMENT_DIR}/${BAND_NAME}/saved_model" \
        --format saved_model \
        --log-dir "/home/gleb/Documents/SignalClassifierGCloud/logs/${EXPERIMENT_NAME}_${BAND_NAME}_predict" \
        --input-length $INPUT_LENGTH 2>/dev/null
#done