# System requirements

- [Python 3.5.8](https://www.python.org/downloads/release/python-358/)
- [PIP](https://pypi.org/project/pip/)
- [Google Cloud SDK](https://cloud.google.com/sdk/install)

It is advised, that you run application's modules with the help of
virtual environments tools. Namely [PyEnv](https://github.com/pyenv/pyenv)
in order to provide an environment for Python version and
[Virtual environment](https://docs.python.org/3/tutorial/venv.html)
libraries' separation.

# Installation

In order to install necessary dependencies, while in the application's root,
execution of following command is required: 
```shell script
$ pip install .
```

## Troubleshooting

In case, that any errors related to Google Cloud libraries or
non-existence of a global function `sort` would occur during an installation,
please execute following commands in order to update PIP and `setuptools`:

```shell script
$ pip install --upgrade pip
$ pip install --upgrade setuptools
```

# Running

Run following while in the application's root (and with the virtual environment
enabled, if installed):

## Unit tests

Runs all available unit tests.

```shell script
$ python -m unittest discovery
```

## Utilities

### Data separator

Separates randomly specified samples to training and test data within each
existing class with a given ratio.

```shell script
$ python -m util.data_separator [INPUT-DIR] [OUTPUT-DIR] [RATIO] 
```

* `INPUT-DIR` is a directory containing files with samples (see 
    [Samples input format](#Samples input format)) separated accordingly
    to classes, to which they belong.
* `OUTPUT-DIR` is a directory, where separated samples will be saved.
    One subdirectory will be created. Current timestamp will be used as
    its name. In that directory two more will be created: `train` and `test`
    containing training and test samples accordingly.
* `RATIO` is a ratio of training samples count to test samples count in
    each present class. It is interpreted as a floating point number.
    
The only output should be the maximum value encountered (used when setting up a
[Trainer](#trainer) module)

## Trainer

```shell script
$ python -m trainer [-h] [--job-dir JOB_DIR] [--model-path MODEL_PATH]
                    [--format [{hdf5,saved_model}]] [--map [MAP [MAP ...]]] [--fft]
                    [--input-length INPUT_LENGTH] [--model-type [{dense,cnn,rnn}]]
                    [--log-dir [LOG_DIR]] [--epochs [EPOCHS]]
                    [--max-possible-value [MAX_POSSIBLE_VALUE]]
```

### Options

* `-h`, `--help` - show this help message and exit
* `--job-dir` - path to the directory containing samples files
* `--model-path` - path to the model. File in case of HDF5 format or
    directory, when Saved model format is selected (see
    `--model-type` option)
* `--format [{hdf5,saved_model}]` - Model format used to save model (trainer)
    or to load it (predictor)
* `--map` - instructions for a Remapping input transformer in a
    following format: x[1].y[1] x[2].y[2] ... x[N], y[N].
    Where x[i] is an integer representing a class from
    which samples are remapped to a class defined by y[i].
* `--fft` - when present, enables FFT input transformer
* `--input-length` - this option is used to set a fixed length of sequences
    in case, when it is not detectable otherwise (mainly
    in trainer mode)
* `--model-type [{dense,cnn,rnn}]` - neural network design used to implement
    a model
* `--log-dir` - path to the directory, where logs are to be stored. Also
    enables additional logging
* `--resample-factor` - used to force a certain downsampling (decimation)
    factor, when it is preferable to an automatic
    detection based on a target sequence length
* `--epochs` - number of training epochs (if not provided, is set to 5 by default)
* `--max-possible-value` - maximum encountered value, in case, when it is not
    detectable otherwise (e.g. when it is preferable to avoid decimation on
    test data and it is known, that they include a higher maximum value in a
    comparison to a training data set)


## Predictor

```shell script
$ python -m trainer [-h] [--job-dir JOB_DIR] [--model-path MODEL_PATH]
                    [--format [{hdf5,saved_model}]] [--map [MAP [MAP ...]]] [--fft]
                    [--input-length INPUT_LENGTH] [--model-type [{dense,cnn,rnn}]]
                    [--log-dir [LOG_DIR]] [--resample-factor [RESAMPLE_FACTOR]]
```

### Options

* `-h`, `--help` - show this help message and exit
* `--job-dir` - path to the directory containing samples files
* `--model-path` - path to the model. File in case of HDF5 format or
    directory, when Saved model format is selected (see
    `--model-type` option)
* `--format [{hdf5,saved_model}]` - Model format used to save model (trainer)
    or to load it (predictor)
* `--map` - instructions for a Remapping input transformer in a
    following format: x[1].y[1] x[2].y[2] ... x[N], y[N].
    Where x[i] is an integer representing a class from
    which samples are remapped to a class defined by y[i].
* `--fft` - when present, enables FFT input transformer
* `--input-length` - this option is used to set a fixed length of sequences
    in case, when it is not detectable otherwise (mainly
    in trainer mode)
* `--model-type [{dense,cnn,rnn}]` - neural network design used to implement
    a model
* `--log-dir` - path to the directory, where logs are to be stored. Also
    enables additional logging
* `--resample-factor` - used to force a certain downsampling (decimation)
    factor, when it is preferable to an automatic
    detection based on a target sequence length

#### Sample input structure

```
input_dir
├── class_0_samples.csv
├── class_1_samples.csv
├── class_2_samples.csv
├── class_3_samples.csv
├── class_4_samples.csv
├── class_5_samples.csv
├── class_6_samples.csv
└── class_7_samples.csv
``` 

#### Sample output structure

```
output_dir
└── 2020-01-09_16:52:42
    ├── test
    │   ├── class_0_samples.csv
    │   ├── class_1_samples.csv
    │   ├── class_2_samples.csv
    │   ├── class_3_samples.csv
    │   ├── class_4_samples.csv
    │   ├── class_5_samples.csv
    │   ├── class_6_samples.csv
    │   └── class_7_samples.csv
    └── train
        ├── class_0_samples.csv
        ├── class_1_samples.csv
        ├── class_2_samples.csv
        ├── class_3_samples.csv
        ├── class_4_samples.csv
        ├── class_5_samples.csv
        ├── class_6_samples.csv
        └── class_7_samples.csv
```

# Samples input format

Samples come in a [CSV files](https://tools.ietf.org/html/rfc4180), where comma
(`,`) is used as a column separator and header is present on the first line of
each file. Those files are required to have at least two columns with names `ID` and
`rawValue`. Each file represents a class of samples. Each line - a value in a sample.

This behaviour can be altered as necessary by modifying the
`util.fs_util.samples_loader.samples_loader` module.

## Sample file structure

```
ID,rawValue
36,9334920.0
36,9335390.0
36,9335840.0
...
``` 

# Google Cloud AI Platform Integration

In order to run training or prediction using a Google Cloud AI Platform clusters
you should have following prerequisites:
* [an active project on Google Cloud](https://cloud.google.com/resource-manager/docs/creating-managing-projects),
* [enabled AI Platform API](https://console.cloud.google.com/flows/enableapi?apiid=ml.googleapis.com,compute_component),
* [a Google Storage bucket created](https://cloud.google.com/storage/docs/creating-buckets).
* [downloaded Google service account credentials](https://cloud.google.com/iam/docs/service-accounts) to `trainer/gcs_credentials.json`

## Training 

Then following steps are required:
1. [Uploading of the separated training and test data to
the bucket](https://cloud.google.com/storage/docs/uploading-objects).
2. Login and pairing with the created object is required for Google
Cloud SDK in order to proceed
(see [gcloud init](https://cloud.google.com/sdk/gcloud/reference/init)). Set up
a default region closest to your physical location supporting both training and
prediction (see [Regions](https://cloud.google.com/ml-engine/docs/regions)).
3. Setup `upload.sh` accordingly to your bucket's credentials.

After that you should be able to send a training job with the `upload.sh` script.
Progress can be monitored in the Google Cloud console web GUI or with the
following command:

```shell script
$ gcloud ai-platform jobs stream-logs [JOB_NAME]
```

## Prediction

In order to run predictor on Google Cloud:
1. Predictor should be packed and deployed with `$ ./pack_predictor.sh [BUCKET_URL]`
2. A trained model should be deployed and version created:

```shell script
$ ./create_version.sh [MODEL_NAME] [BUCKET_URL] [GS_PATH_TO_TRAINED_MODEL]
                      [GS_PATH_TO_DEPLOYED_PREDICTOR]
``` 

* `MODEL_NAME` - unique name to be used as an identifier for a deployed model
* `BUCKET_URL` - URL in format `gs://bucket-name`
* `GS_PATH_TO_TRAINED_MODEL` - absolute path in Google storage bucket leading
    to the trained model `/path/to/model`
* `GS_PATH_TO_DEPLOYED_PREDICTOR` - absolute path in Google storage bucket leading
    to the deployed predictor `/path/to/predictor_package.tar.gz`
    
Deployed version (model and predictor) can be run for test data located in bucket
as described [here](https://cloud.google.com/ml-engine/docs/online-predict).

### Troubleshooting

Possible errors may occur due to the Google Cloud model size quota. Model can be
reduced or alternatively quote can be raised.
