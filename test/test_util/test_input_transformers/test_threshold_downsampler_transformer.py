from unittest import TestCase
from unittest.mock import patch, MagicMock

from util.input_transformers import ThresholdDownsamplerTransformer


class TestThresholdDownsamplerTransformer(TestCase):
    @patch('util.input_transformers.downsampling_input_transformer.DownsamplingInputTransformer.transform')
    def test_transform_positive_auto_downsample_factor(self, downsampling_input_transformer_transform_mock: MagicMock):
        downsampling_input_transformer_transform_mock.return_value = {}
        transformer = ThresholdDownsamplerTransformer(50)
        transformer.transform({1: [list(range(80))], 2: [list(range(50))]})
        self.assertEqual(transformer._downsampling_factor, 2)

    @patch('util.input_transformers.downsampling_input_transformer.DownsamplingInputTransformer.transform')
    def test_transform_positive_force_downsample_factor(self, downsampling_input_transformer_transform_mock: MagicMock):
        downsampling_input_transformer_transform_mock.return_value = {}
        transformer = ThresholdDownsamplerTransformer(50, 3)
        transformer.transform({1: [list(range(80))], 2: [list(range(50))]})
        self.assertEqual(transformer._downsampling_factor, 3)

    @patch('util.input_transformers.downsampling_input_transformer.DownsamplingInputTransformer.transform')
    def test_transform_negative_zero_length(self, downsampling_input_transformer_transform_mock: MagicMock):
        downsampling_input_transformer_transform_mock.return_value = {}
        transformer = ThresholdDownsamplerTransformer(0)
        self.assertRaises(ZeroDivisionError, transformer.transform, {1: [list(range(80))], 2: [list(range(50))]})

    @patch('util.input_transformers.downsampling_input_transformer.DownsamplingInputTransformer.transform')
    def test_transform_negative_negative_length(self, downsampling_input_transformer_transform_mock: MagicMock):
        downsampling_input_transformer_transform_mock.return_value = {}
        transformer = ThresholdDownsamplerTransformer(-1)
        self.assertRaises(ValueError, transformer.transform, {1: [list(range(80))], 2: [list(range(50))]})

