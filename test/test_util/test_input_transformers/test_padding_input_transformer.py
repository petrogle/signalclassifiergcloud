import os
from unittest import TestCase
from numpy.testing import assert_equal

from util.input_transformers import PaddingInputTransformer


class TestPaddingInputTransformer(TestCase):
    def test_transform_single_sequence_positive_adding_from_both_sides_equally(self):
        transformer = PaddingInputTransformer(4)
        assert_equal(transformer.transform_single_sequence([1, 2]), [0, 1, 2, 0])

    def test_transform_single_sequence_positive_adding_from_both_sides_unequally(self):
        transformer = PaddingInputTransformer(5)
        assert_equal(transformer.transform_single_sequence([1, 2]), [0, 1, 2, 0, 0])

    def test_transform_single_sequence_positive_empty(self):
        transformer = PaddingInputTransformer(1)
        assert_equal(transformer.transform_single_sequence([]), [0])

    def test_transform_single_sequence_negative_too_short(self):
        transformer = PaddingInputTransformer(2)
        self.assertRaises(ValueError, transformer.transform_single_sequence, [1, 2, 3])

    def test_transform_positive_given_length(self):
        transformer = PaddingInputTransformer(3)
        assert_equal(
            transformer.transform({1: [[1, 2], [1]], 2: [[3, 4], []]}),
            {1: [[1, 2, 0], [0, 1, 0]], 2: [[3, 4, 0], [0, 0, 0]]}
        )

    def test_transform_positive_auto_length(self):
        transformer = PaddingInputTransformer()
        assert_equal(
            transformer.transform({1: [[1, 2, 3], [1]], 2: [[3, 4], []]}),
            {1: [[1, 2, 3], [0, 1, 0]], 2: [[3, 4, 0], [0, 0, 0]]}
        )

    def test_transform_negative_too_short(self):
        transformer = PaddingInputTransformer(1)
        self.assertRaises(
            ValueError,
            transformer.transform,
            {1: [[1, 2], [1]], 2: [[3, 4], []]}
        )
