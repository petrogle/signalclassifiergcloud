from unittest import TestCase

from util.input_transformers import RemappingTransformer


class TestRemappingTransformer(TestCase):

    def test_transform_positive_do_nothing(self):
        transformer = RemappingTransformer({0: 0, 1: 1, 2: 2})
        self.assertEqual(
            transformer.transform({
                0: [[1, 2, 3], [4, 5, 6]],
                1: [[7, 8, 9], [0, 1, 2]],
                2: [[3, 4, 5], [6, 7, 8]],
            }),
            {
                0: [[1, 2, 3], [4, 5, 6]],
                1: [[7, 8, 9], [0, 1, 2]],
                2: [[3, 4, 5], [6, 7, 8]],
            }
        )

    def test_transform_positive_change_class(self):
        transformer = RemappingTransformer({0: 0, 1: 1, 2: 3})
        self.assertEqual(
            transformer.transform({
                0: [[1, 2, 3], [4, 5, 6]],
                1: [[7, 8, 9], [0, 1, 2]],
                2: [[3, 4, 5], [6, 7, 8]],
            }),
            {
                0: [[1, 2, 3], [4, 5, 6]],
                1: [[7, 8, 9], [0, 1, 2]],
                3: [[3, 4, 5], [6, 7, 8]],
            }
        )

    def test_transform_positive_merge_class(self):
        transformer = RemappingTransformer({0: 0, 1: 1, 2: 1})
        self.assertEqual(
            transformer.transform({
                0: [[1, 2, 3], [4, 5, 6]],
                1: [[7, 8, 9], [0, 1, 2]],
                2: [[3, 4, 5], [6, 7, 8]],
            }),
            {
                0: [[1, 2, 3], [4, 5, 6]],
                1: [[7, 8, 9], [0, 1, 2], [3, 4, 5], [6, 7, 8]],
            }
        )

    def test_transform_negative_missing_class(self):
        transformer = RemappingTransformer({0: 0, 1: 1})
        self.assertRaises(ValueError, transformer.transform, {
            0: [[1, 2, 3], [4, 5, 6]],
            1: [[7, 8, 9], [0, 1, 2]],
            2: [[3, 4, 5], [6, 7, 8]],
        })

    def test_transform_negative_excessive_class(self):
        transformer = RemappingTransformer({0: 0, 1: 1, 2: 2, 3: 3})
        self.assertRaises(ValueError, transformer.transform, {
            0: [[1, 2, 3], [4, 5, 6]],
            1: [[7, 8, 9], [0, 1, 2]],
            2: [[3, 4, 5], [6, 7, 8]],
        })
