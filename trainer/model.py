import tensorflow as tf

EMBEDDING_VECTOR_SIZE = 16
CONVOLUTION_KERNEL_SIZE = 3
POOL_SIZE = 3


def save_model(model: tf.keras.models.Model, path: str):
    tf.keras.models.save_model(model, path)


def build_model(min_value: int, max_value: int, sequence_length: int, classes_count: int):
    model = tf.keras.Sequential([
        tf.keras.layers.Embedding(max_value - min_value + 1, EMBEDDING_VECTOR_SIZE, input_length=sequence_length),
        tf.keras.layers.Convolution1D(EMBEDDING_VECTOR_SIZE, kernel_size=CONVOLUTION_KERNEL_SIZE, padding='same'),
        tf.keras.layers.MaxPooling1D(POOL_SIZE),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(classes_count, activation='sigmoid')
    ])

    model.compile(
        optimizer=tf.keras.optimizers.Adam(),
        loss='sparse_categorical_crossentropy',
        metrics=['accuracy']
    )

    return model
