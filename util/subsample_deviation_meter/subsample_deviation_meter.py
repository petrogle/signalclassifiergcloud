from typing import Sequence, Dict

from util.input_transformers import DownsamplingInputTransformer


class SubsampleDeviationMeter:
    factors = ...  # type: Sequence[int]

    def __init__(self, factors: Sequence[int]):
        self.factors = factors

    def calculate_relative_deviation(self, values: Sequence[int]) -> Dict[int, float]:
        deviation_by_factor = {}
        max_value = max(values)
        min_value = min(values)
        range = max_value - min_value

        for factor in self.factors:
            transformer = DownsamplingInputTransformer(factor)
            downsampled_values = transformer.transform_single_sequence(values)
            upper_outer_deviation = max(downsampled_values) - max_value
            lower_outer_deviation = min_value - min(downsampled_values)
            deviation_by_factor[factor] = max(
                upper_outer_deviation if upper_outer_deviation > 0 else 0,
                lower_outer_deviation if lower_outer_deviation > 0 else 0
            )
            if deviation_by_factor[factor] / range > 0.5:
                pass

        return {k: v / range for k, v in deviation_by_factor.items()}
