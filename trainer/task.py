import logging
import os
import sys
import warnings

from trainer.constants import GOOGLE_CREDENTIALS_PATH

warnings.simplefilter(action='ignore', category=FutureWarning)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
if os.path.exists(GOOGLE_CREDENTIALS_PATH):
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = GOOGLE_CREDENTIALS_PATH
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

from trainer.services import Services


task_class = Services.task_class()
task_class.load().run()
