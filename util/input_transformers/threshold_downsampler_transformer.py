from math import ceil
from typing import Sequence, Dict, Any, Optional

from util.input_transformers.downsampling_input_transformer import DownsamplingInputTransformer


class ThresholdDownsamplerTransformer(DownsamplingInputTransformer):
    MAX_OUTER_DEVIATION_AA = 0.09225
    MAX_OUTER_DEVIATION_MULTIPLIER = 2

    _array_length = ...  # type: int
    _force_downsample_factor = ...  # type: Optional[int]

    def __init__(self, required_input_length: int, force_downsample_factor: Optional[int] = None):
        super().__init__()
        self._force_downsample_factor = force_downsample_factor
        self._array_length = required_input_length

    def transform(self, seqs_of_seqs_by_class_id: Dict[int, Sequence[Sequence[Any]]]) -> Dict[int, Sequence[Sequence[Any]]]:
        if self._force_downsample_factor is None:
            max_sample_length = max(
                max(len(seqs) for seqs in seqs_of_seqs)
                for class_id, seqs_of_seqs in seqs_of_seqs_by_class_id.items()
            )
            self.set_resample_factor_by_max_sample_length(max_sample_length)
        else:
            self._downsampling_factor = self._force_downsample_factor
        return super(ThresholdDownsamplerTransformer, self).transform(seqs_of_seqs_by_class_id)

    def set_resample_factor_by_max_sample_length(self, max_sample_length: int) -> None:
        if self._array_length < 0:
            raise ValueError('Array length cannot be a negative number')
        max_sample_length *= self._get_max_sample_length_multiplier()
        if max_sample_length < self._array_length:
            return
        self._downsampling_factor = ceil(max_sample_length / self._array_length)

    @classmethod
    def _get_max_sample_length_multiplier(cls) -> float:
        return 1 + cls.MAX_OUTER_DEVIATION_AA * cls.MAX_OUTER_DEVIATION_MULTIPLIER
