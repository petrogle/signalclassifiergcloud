#!/bin/bash

#nn_types=('dense' 'cnn' 'rnn')
nn_types=('cnn')
job_dir=$1
model_dir=$2
log_dir=$3
max_possible_value=91123
python_executable='/home/gleb/.pyenv/versions/3.5.8/bin/python'

for epochs in {16..18}; do
    echo -e "$epochs epochs:\n\n"
    for nn_type in "${nn_types[@]}"; do
        echo 'Running trainer...';
        $python_executable -m trainer.task \
            --job-dir "$job_dir/train" \
            --model-path "$model_dir/$epochs/$nn_type" \
            --log-dir "$log_dir/$nn_type/$epochs/train" \
            --model-type "$nn_type" \
            --max-possible-value $max_possible_value \
            --epochs $epochs \
            --format saved_model;

        if [ $? -ne 0 ]; then
            echo 'Trainer failed' 1>&2;
            exit 1;
        fi

        echo 'Running predictor...'
        $python_executable -m predictor.task \
            --job-dir "$job_dir/test" \
            --model-path "$model_dir/$epochs/$nn_type" \
            --format saved_model \
            --log-dir "$log_dir/$nn_type/$epochs/test";

        if [ $? -ne 0 ]; then
            echo 'Predictor failed' 1>&2;
            exit 1;
        fi
    done;
done;
