from dependency_injector import containers, providers
from google.cloud.storage import Client

from trainer.constants import GOOGLE_CREDENTIALS_PATH
from util.fs_util.samples_loader import EnumeratedClassesSamplesLoader, SamplesLoader
from util.fs_util.samples_loader.gcloud_samples_loader import GCloudSamplesLoader
from util.fs_util.samples_loader.universal_samples_loader import UniversalSamplesLoader

common_services = containers.DynamicContainer()
try:
    common_services.cloud_storage_client = providers.Object(Client.from_service_account_json(GOOGLE_CREDENTIALS_PATH))
except FileNotFoundError:
    common_services.cloud_storage_client = providers.Object(Client())
common_services.local_samples_loader = providers.Singleton(SamplesLoader)
common_services.gcloud_samples_loader = providers.Singleton(GCloudSamplesLoader, common_services.cloud_storage_client(),
                                                            common_services.local_samples_loader())
common_services.universal_samples_loader = providers.Singleton(UniversalSamplesLoader,
                                                               common_services.local_samples_loader(),
                                                               common_services.gcloud_samples_loader())
common_services.samples_loader = providers.Singleton(EnumeratedClassesSamplesLoader,
                                                     common_services.universal_samples_loader())
