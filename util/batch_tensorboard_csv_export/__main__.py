"""
This module is based on the StackOverflow answer https://stackoverflow.com/a/52095336/6045144
"""

import os
import numpy as np
import pandas as pd
import warnings

from collections import defaultdict
from tensorboard.backend.event_processing.event_accumulator import EventAccumulator
from sys import argv


def tabulate_events(dpath, dirs):
    summary_iterators = [EventAccumulator(os.path.join(dpath, dname)).Reload() for dname in dirs]
    tags = summary_iterators[0].Tags()['scalars']
    for it in summary_iterators:
        assert it.Tags()['scalars'] == tags
    out = defaultdict(list)
    steps = []
    for tag in tags:
        steps = [e.step for e in summary_iterators[0].Scalars(tag)]
        for events in zip(*[acc.Scalars(tag) for acc in summary_iterators]):
            assert len(set(e.step for e in events)) == 1
            out[tag].append([e.value for e in events])
    return out, steps


def to_csv(dpath, epochs: str):
    dirs = [directory for directory in os.listdir(dpath) if directory.endswith('_' + epochs)]
    d, steps = tabulate_events(dpath, dirs)
    tags, values = zip(*d.items())
    np_values = np.array(values)
    for index, tag in enumerate(tags):
        df = pd.DataFrame(np_values[index], index=steps, columns=dirs)
        file_path = get_file_path(dpath, tag + '_' + epochs)
        df.to_csv(file_path)
        print('Saved to %s' % file_path)


def get_file_path(dpath, tag):
    file_name = tag.replace("/", "_") + '.csv'
    folder_path = os.path.join(dpath, 'csv')
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    return os.path.join(folder_path, file_name)


if __name__ == '__main__':
    warnings.simplefilter(action='ignore', category=FutureWarning)
    for i in range(1, 21):
        print('%d epoch(s)...' % i)
        to_csv(argv[1], str(i))
