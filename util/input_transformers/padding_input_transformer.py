from typing import Dict, Any, Optional, Sequence

import numpy as np
import tensorflow as tf

from util.input_transformers.base_single_sequence_based_transformer import BaseSingleSequenceBasedTransformer


class PaddingInputTransformer(BaseSingleSequenceBasedTransformer):
    _required_input_length = ...  # type: Optional[int]
    length = ...  # type

    def __init__(self, required_input_length: int = None):
        self._required_input_length = required_input_length

    def transform(self, seqs_of_seqs_by_class_id: Dict[int, Sequence[Sequence[Any]]]) -> Dict[int, Sequence[Sequence[Any]]]:
        if self._required_input_length is None:
            self._required_input_length = max(
                max(len(sample) for sample in samples)
                for event_id, samples in seqs_of_seqs_by_class_id.items()
            )
            print("Padding length set to %d" % self._required_input_length)
        return super(PaddingInputTransformer, self).transform(seqs_of_seqs_by_class_id)

    def transform_single_sequence(self, sequence: Sequence[Any]) -> Sequence[Any]:
        if self._required_input_length is None:
            raise AttributeError('Required input length is not set')
        diff = self._required_input_length - len(sequence)
        if diff != 0:
            new_sequence = tf.pad(sequence, [[int(np.floor(diff / 2.0)), int(np.ceil(diff / 2.0))]])
            sess = tf.compat.v1.Session()
            with sess.as_default():
                sequence = np.asarray(new_sequence.eval())
        return sequence
