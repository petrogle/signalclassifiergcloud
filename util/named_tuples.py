from collections import namedtuple
from typing import List, Any, Dict

import numpy as np

EventsLabels = namedtuple('EventsLabels', ['events', 'labels'])


def make_events_labels_from_dict(input_by_event_id: Dict[str, List[List[int]]]) -> EventsLabels:
    events_l = []
    labels_l = []
    for event_id, inputs in input_by_event_id.items():
        events_l.extend(inputs)
        labels_l.extend([event_id] * len(inputs))
    return EventsLabels(events_l, labels_l)


def make_np_events_labels_from_dict(input_by_event_id: Dict[str, List[List[int]]]) -> EventsLabels:
    list_events_labels = make_events_labels_from_dict(input_by_event_id)
    return EventsLabels(np.asarray(list_events_labels.events), np.asarray(list_events_labels.labels))

