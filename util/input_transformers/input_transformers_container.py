from dependency_injector import containers, providers
from dependency_injector.providers import Configuration

from . import *


class InputTransformersContainer(containers.DeclarativeContainer):
    config = providers.Configuration('config')  # type: Configuration

    padding = providers.Factory(PaddingInputTransformer, config.required_input_length)
    events_remapping = providers.Factory(RemappingTransformer, config.class_mapping)
    fft = providers.Factory(FftTransformer)
    normalizing = providers.Factory(NormalizingInputTransformer, config.max_possible_value)
    downsampler = providers.Factory(ThresholdDownsamplerTransformer, config.required_input_length, config.force_downsample_factor)
