#!/usr/bin/env bash

MODEL_NAME=$1
BUCKET="gs://signal-classificator-mlengine"
REGION="europe-west1" # europe-west4 if does not work
OUTPUT_PATH=${BUCKET}/output/${MODEL_NAME}

echo "Creating model..."
gcloud ai-platform models create $MODEL_NAME \
    --regions $REGION

echo "Listing deployed models..."
gcloud ai-platform models list