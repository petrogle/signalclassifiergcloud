from typing import Sequence, Any

from util.input_transformers.base_single_sequence_based_transformer import BaseSingleSequenceBasedTransformer


class NormalizingInputTransformer(BaseSingleSequenceBasedTransformer):
    _min_possible_value = ...  # type: float
    _max_possible_value = ...  # type: float

    def __init__(self, max_possible_value: int, min_possible_value: int = 0):
        self._min_possible_value = float(min_possible_value)
        self._max_possible_value = float(max_possible_value)

    def transform_single_sequence(self, sequence: Sequence[Any]) -> Sequence[Any]:
        result_sequence = []
        for value in sequence:
            if self._max_possible_value < value:
                raise ValueError('Encountered value higher than the maximum')
            result_sequence.append(
                (value - self._min_possible_value) /
                (self._max_possible_value - self._min_possible_value)
            )
        return result_sequence
