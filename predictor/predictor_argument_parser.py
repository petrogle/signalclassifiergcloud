from util.argument_parser import BaseArgumentParser


class PredictorArgumentParser(BaseArgumentParser):

    def setup(self):
        super().setup()
        self.add_argument("--resample-factor", type=int, nargs="?", default=1, help="""Used to force a certain
            downsampling (decimation) factor, when it is preferable to an automatic detection based on a target
            sequence length""")
