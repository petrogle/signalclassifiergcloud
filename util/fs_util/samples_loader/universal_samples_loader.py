from typing import Union, TextIO, Pattern, Optional

from google.cloud.storage import Blob

from util.fs_util.functions import is_google_cloud_storage_address
from util.fs_util.samples_loader.base_samples_loader import BaseSamplesLoader


class UniversalSamplesLoader(BaseSamplesLoader):
    _local_samples_loader = ...  # type: BaseSamplesLoader
    _gcloud_samples_loader = ...  # type: BaseSamplesLoader
    _last_used_samples_loader = None  # type: Optional[BaseSamplesLoader]

    def __init__(self, local_samples_loader: BaseSamplesLoader, gcloud_samples_loader: BaseSamplesLoader):
        self._gcloud_samples_loader = gcloud_samples_loader
        self._local_samples_loader = local_samples_loader

    def load_single_file(self, csv_file: Union[TextIO, Blob]):
        if isinstance(csv_file, Blob):
            self._last_used_samples_loader = self._gcloud_samples_loader
        else:
            self._last_used_samples_loader = self._local_samples_loader
        return self._last_used_samples_loader.load_single_file(csv_file)

    def load_from_dir(self, directory: str, file_name_pattern: Pattern):
        if is_google_cloud_storage_address(directory):
            self._last_used_samples_loader = self._gcloud_samples_loader
        else:
            self._last_used_samples_loader = self._local_samples_loader
        return self._last_used_samples_loader.load_from_dir(directory, file_name_pattern)

    @property
    def max_encountered_value(self) -> int:
        return self._last_used_samples_loader.max_encountered_value

    @property
    def longest_encountered_sequence(self) -> int:
        return self._last_used_samples_loader.longest_encountered_sequence
