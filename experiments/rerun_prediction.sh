#!/bin/bash

EXPERIMENT_DIR=$1
PROJECT_ROOT=`realpath .`

TEST_DIR=`find "${EXPERIMENT_DIR}" -type d -name test | sort | head -1`

if [ -z "$2" ]; then
  INPUT_LENGTH=`find "${EXPERIMENT_DIR}" -path '*/train/*_Samples.csv' | xargs -I{} sh -c 'cat {} | tail -n+2 | cut -d, -f 1 | sort | uniq -c' | sort -h | tail -1 | grep -Po '\d+(?= )'`
else
  INPUT_LENGTH=$2
fi

echo "Running: ${PROJECT_ROOT}/venv/bin/python -m predictor.task --job-dir \"${TEST_DIR}\" --model-path \"${EXPERIMENT_DIR}/saved_model\" --format saved_model --input-length $INPUT_LENGTH"

$PROJECT_ROOT/venv/bin/python -m predictor.task --job-dir "${TEST_DIR}" \
	--model-path "${EXPERIMENT_DIR}/saved_model" \
	--format saved_model \
	--input-length $INPUT_LENGTH
