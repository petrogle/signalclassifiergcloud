import tensorflow as tf

from trainer.model_builder.base_model_builder import BaseModelBuilder


class DenseModelBuilder(BaseModelBuilder):
    FIRST_LAYER_NEURONS_COUNT = 512  # type: int
    SECOND_LAYER_NEURONS_COUNT = 256  # type: int

    def _do_build(self) -> tf.keras.Model:
        model = tf.keras.Sequential([
            tf.keras.layers.Dense(self.FIRST_LAYER_NEURONS_COUNT, activation=tf.nn.relu),
            tf.keras.layers.Dense(self.SECOND_LAYER_NEURONS_COUNT, activation=tf.nn.relu),
            tf.keras.layers.Dense(self.classes_count, activation=tf.nn.softmax),
        ])

        return model
