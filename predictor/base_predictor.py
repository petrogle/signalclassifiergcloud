from abc import ABC, abstractmethod
from typing import List, Dict

from predictor.google_predictor import GooglePredictor


class BasePredictor(GooglePredictor, ABC):

    @abstractmethod
    def predict_single_instance(self, instance: List[int]) -> int:
        pass

    @abstractmethod
    def setup(self, kwargs: Dict):
        pass

    @classmethod
    def from_path(cls, model_dir: str) -> 'BasePredictor':
        return super(BasePredictor).from_path(model_dir)
