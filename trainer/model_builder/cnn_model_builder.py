import tensorflow as tf

from trainer.model_builder.base_model_builder import BaseModelBuilder


class CnnModelBuilder(BaseModelBuilder):
    EMBEDDING_VECTOR_SIZE = 2
    CONVOLUTION_KERNELS_NUMBER = 2
    CONVOLUTION_KERNEL_SIZE = 2
    POOL_SIZE = 2
    MIN_POSSIBLE_VALUE = 0

    sequence_length = ...  # type: int
    max_possible_value = ...  # type: int

    def __init__(self, classes_count: int, sequence_length: int, max_possible_value: int):
        super().__init__(classes_count)
        self.sequence_length = sequence_length
        self.max_possible_value = max_possible_value

    def _do_build(self) -> tf.keras.Model:
        model = tf.keras.Sequential([
            tf.keras.layers.Embedding(
                self.max_possible_value - self.MIN_POSSIBLE_VALUE + 1,
                self.EMBEDDING_VECTOR_SIZE,
                input_length=self.sequence_length
            ),
            tf.keras.layers.Convolution1D(
                filters=self.CONVOLUTION_KERNELS_NUMBER,
                kernel_size=self.CONVOLUTION_KERNEL_SIZE,
                padding='same'
            ),
            tf.keras.layers.MaxPooling1D(self.POOL_SIZE),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(self.classes_count, activation='softmax'),
        ])
        return model
