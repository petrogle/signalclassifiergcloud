from abc import ABCMeta, abstractmethod


class BaseSamplesLoader(metaclass=ABCMeta):
    @abstractmethod
    def load_from_dir(self, directory, file_name_pattern):
        """
        :param directory: path to the directory containing files, where each file has events of one category
        :param file_name_pattern: pattern, with which file's name should comply in order to be processed
        :returns dictionary, where the key is an event's name and value is a list of lists containing measured values
        """
        pass

    @abstractmethod
    def load_single_file(self, csv_file):
        """
        :param csv_file: path to the file containing events of a single category
        :returns list of lists containing measured values
        """
        pass

    @property
    @abstractmethod
    def max_encountered_value(self) -> int:
        pass

    @property
    @abstractmethod
    def longest_encountered_sequence(self) -> int:
        pass
