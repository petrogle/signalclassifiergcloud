from numpy.testing import assert_equal, assert_almost_equal
from unittest import TestCase

from util.input_transformers import NormalizingInputTransformer


class TestNormalizingInputTransformer(TestCase):
    def test_transform_single_sequence_positive(self):
        transformer = NormalizingInputTransformer(10)
        assert_equal(transformer.transform_single_sequence([0, 5, 10]), [0, 0.5, 1])

    def test_transform_single_sequence_positive_empty(self):
        transformer = NormalizingInputTransformer(10)
        assert_equal(transformer.transform_single_sequence([]), [])

    def test_transform_single_sequence_positive_rounding(self):
        transformer = NormalizingInputTransformer(9)
        assert_almost_equal(transformer.transform_single_sequence([0, 1, 3, 9]), [0, 0.111, 0.333, 1], 3)

    def test_transform_single_sequence_negative_too_high(self):
        transformer = NormalizingInputTransformer(3)
        self.assertRaises(ValueError, transformer.transform_single_sequence, [0, 4])

