#!/bin/bash

DATA_DIR=$1
DATA_ROOT=$(dirname ${DATA_DIR})
DATA_DIR_NAME=$(basename ${DATA_DIR})
EXPERIMENT_NAME="${DATA_DIR_NAME}_ect_$(date +%Y-%m-%d_%H:%M:%S)"
EXPERIMENT_DIR="${DATA_ROOT}/${EXPERIMENT_NAME}"
PROJECT_ROOT="/home/gleb/Documents/SignalClassifierGCloud"
RATIO=4

echo "Separating..."
MAX_VALUE=$(
    $PROJECT_ROOT/venv/bin/python -m util.data_separator.task "${DATA_DIR}" "${EXPERIMENT_DIR}" $RATIO \
    | grep -oP '(?<=Max value: )\d+'
)
TRAIN_DIR=`find "${EXPERIMENT_DIR}" -type d -name train`
TEST_DIR=`find "${EXPERIMENT_DIR}" -type d -name test`
echo "Max value: ${MAX_VALUE}"

for EPOCHS in `seq 20`; do
    echo "Training ${EPOCHS} epochs..."
    TRAIN_OUTPUT=$(
        $PROJECT_ROOT/venv/bin/python -m trainer.task --job-dir "${TRAIN_DIR}" \
            --model-path "${EXPERIMENT_DIR}/${EPOCHS}_epochs_saved_model" \
            --format saved_model \
            --model-type cnn \
            --epochs $EPOCHS \
            --max-possible-value $MAX_VALUE \
            --log-dir "${PROJECT_ROOT}/logs/${EXPERIMENT_NAME}_${EPOCHS}/fit" \
            2>&1 \
            | grep -v 'FutureWarning' \
            | tee /dev/tty
    )
    INPUT_LENGTH=$(echo $TRAIN_OUTPUT | grep -oP '(?<=Padding length set to )\d+')

    if [ ${PIPESTATUS[0]} -ne 0 ]; then
        echo "Training failed. Exiting"
        exit 1
    fi

    $PROJECT_ROOT/venv/bin/python -m predictor.task \
        --job-dir "${TEST_DIR}" \
        --model-path "${EXPERIMENT_DIR}/${EPOCHS}_epochs_saved_model" \
        --format saved_model \
        --log-dir "${PROJECT_ROOT}/logs/${EXPERIMENT_NAME}_${EPOCHS}/predict" \
        --input-length $INPUT_LENGTH \
        | grep -v 'FutureWarning'

    if [ ${PIPESTATUS[0]} -ne 0 ]; then
        echo "Prediction failed. Exiting"
        exit 1
    fi

done
