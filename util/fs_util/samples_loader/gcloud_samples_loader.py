import collections
import io
import logging
from typing import Optional, List, Dict, Pattern

from google.cloud.storage import Client, Bucket, Blob

from util.fs_util.functions import parse_google_cloud_storage_address
from util.fs_util.samples_loader.base_samples_loader import BaseSamplesLoader


class GCloudSamplesLoader(BaseSamplesLoader):
    ENCODING = 'utf-8'

    _samples_loader = ...  # type: BaseSamplesLoader
    _cloud_storage_client = ...  # type: Client
    _bucket = None  # type: Optional[Bucket]
    _longest_encountered_sequence = 0  # type: int

    def __init__(self, cloud_storage_client: Client, samples_loader: BaseSamplesLoader):
        self._samples_loader = samples_loader
        self._cloud_storage_client = cloud_storage_client

    def load_single_file(self, csv_file: Blob) -> Dict[str, List[List[int]]]:
        string = csv_file.download_as_string().decode(self.ENCODING)
        file_contents = self._samples_loader.load_single_file(io.StringIO(string))
        return file_contents

    def load_from_dir(self, directory: str, file_name_pattern: Pattern):
        try:
            bucket_name, directory = parse_google_cloud_storage_address(directory)
        except ValueError:
            raise NotADirectoryError('Bad directory provided %s' % directory)
        self._bucket = self._cloud_storage_client.get_bucket(bucket_name)
        values = {}  # type: Dict[str, List[List[int]]]
        for blob in self._bucket.list_blobs(prefix=directory):  # type: Blob
            file_name = blob.name.split('/')[-1]
            if not file_name_pattern.match(file_name):
                continue
            logging.info('Processing %s...', file_name)
            values[file_name] = self.load_single_file(blob)
            logging.info('%d events loaded', len(values[file_name]))
        values = collections.OrderedDict(sorted(values.items()))
        return values

    @property
    def max_encountered_value(self) -> int:
        return self._samples_loader.max_encountered_value

    @property
    def longest_encountered_sequence(self) -> int:
        return self._samples_loader.longest_encountered_sequence
