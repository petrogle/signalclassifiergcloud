#!/usr/bin/env bash

for epochs in {1..10}; do
    echo "$epochs epochs"

    venv/bin/python -m trainer.task \
        --job-dir /home/gleb/PycharmProjects/SignalClassificator/EventProcessing/out \
        --model-path /home/gleb/Documents/diplomka/models/model.hdf5 \
        --format hdf5 \
        --epochs ${epochs} 2>\dev\null 1>\dev\null

    venv/bin/python -m predictor.task \
        --job-dir /home/gleb/PycharmProjects/SignalClassificator/EventProcessing/out \
        --model-path /home/gleb/Documents/diplomka/models/model.hdf5 \
        --format hdf5 2>\dev\null | grep Error
done
