import logging
from abc import ABC, abstractmethod

from tensorflow.python.keras import Model, optimizers

from util.fs_util.functions import get_process_memory_usage


class BaseModelBuilder(ABC):
    LOSS_FUNCTION = 'sparse_categorical_crossentropy'
    METRICS = ['accuracy']

    classes_count = ...  # type: int

    def __init__(self, classes_count: int):
        self.classes_count = classes_count

    def build(self) -> Model:
        logging.debug('Memory usage before loading the model %d', get_process_memory_usage())
        model = self._do_build()
        model.compile(optimizers.Adam(), self.LOSS_FUNCTION, self.METRICS)
        logging.debug('Memory usage after loading the model %d', get_process_memory_usage())
        return model

    @abstractmethod
    def _do_build(self) -> Model:
        pass
