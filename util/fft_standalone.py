from sys import stdin, stdout
from typing import Sequence

import numpy as np

from scipy.fftpack import fft

waveform = np.loadtxt(stdin, dtype=np.float)
fft = fft(waveform)  # type: Sequence[complex]

np.savetxt(stdout, [val.real for val in fft], fmt='%f')