from enum import Enum


class ModelTypeEnum(Enum):
    dense = 0
    cnn = 1
    rnn = 2
