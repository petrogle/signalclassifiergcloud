import logging
import re
from argparse import Namespace

from trainer.model_builder.model_factory import ModelFactory
from util.fs_util.samples_loader.base_samples_loader import BaseSamplesLoader
from .model_runner import ModelRunner
from util.argument_parser import BaseArgumentParser
from util.fs_util.model_export_format_enum import ModelExportFormatEnum
from util.fs_util.model_file_writer import BaseModelFileWriter
from util.input_transformers.input_transformer_factory import InputTransformerFactory
from util.model_type_enum import ModelTypeEnum
from util.named_tuples import make_np_events_labels_from_dict, EventsLabels


class TaskClass:
    SAMPLES_FILE_PATTERN = '.+.csv$'

    _argument_parser = ...  # type: BaseArgumentParser
    _samples_loader = ...  # type: BaseSamplesLoader
    _args = ...  # type: Namespace
    _model_factory = ...  # type: ModelFactory
    _model_type = ...  # type: ModelTypeEnum
    _events_labels = ...  # type: EventsLabels
    _model = ...  # type: tf.keras.Model
    _model_runner = ...  # type: ModelRunner
    _file_writer = ...  # type: BaseModelFileWriter

    def __init__(
            self,
            argument_parser: BaseArgumentParser,
            samples_loader: BaseSamplesLoader,
            model_factory: ModelFactory,
            model_runner: ModelRunner
    ):
        self._samples_loader = samples_loader
        self._argument_parser = argument_parser
        self._model_factory = model_factory
        self._args = self._argument_parser.parse_args()
        self._model_type = ModelTypeEnum[self._args.model_type]
        self._model_runner = model_runner
        self._file_writer = BaseModelFileWriter.get_instance(
            self._args.model_path,
            ModelExportFormatEnum[self._args.format]
        )

    def load(self):
        logging.info('Loading samples from %s...', self._args.job_dir)
        input_by_e_id = self._samples_loader.load_from_dir(self._args.job_dir, re.compile(self.SAMPLES_FILE_PATTERN))

        samples_count = sum(len(inputs) for e_id, inputs in input_by_e_id.items())
        if samples_count == 0:
            logging.error('No samples were loaded')
            exit(1)
        logging.info('%d samples loaded', samples_count)
        if self._args.max_possible_value is not None:
            max_possible_value = self._args.max_possible_value
        else:
            max_possible_value = self._samples_loader.max_encountered_value

        logging.info('Applying transformations...')
        transformer_factory = InputTransformerFactory(
            model_type=self._model_type,
            enable_padding=True,
            class_mapping=self._args.map,
            enable_fft=False,
            required_input_length=self._args.input_length,
            force_downsample_factor=None,
            max_possible_value=max_possible_value
        )
        for transformer in transformer_factory.get_transformers_chain():
            logging.info('\t- ' + transformer.__class__.__name__)
            input_by_e_id = transformer.transform(input_by_e_id)

        logging.info('Building model...')
        self._model = self._model_factory.set_params(
            classes_count=len(input_by_e_id),
            sequence_length=len([seqs for e, seqs in input_by_e_id.items()][0][0]),
            max_possible_value=max_possible_value
        ).get_model(self._model_type)

        self._events_labels = make_np_events_labels_from_dict(input_by_e_id)

        return self

    def run(self):
        self._model_runner.fit(self._model, self._events_labels, self._args.epochs)
        if self._args.model_path is not None:
            logging.info('Saving to %s...', self._args.model_path)
            self._file_writer.write(self._model)

