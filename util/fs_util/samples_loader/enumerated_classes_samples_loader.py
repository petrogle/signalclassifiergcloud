import collections
import logging

from typing import Dict, List, Pattern

from .base_samples_loader import BaseSamplesLoader


class EnumeratedClassesSamplesLoader(BaseSamplesLoader):
    _samples_loader = ...  # type: BaseSamplesLoader

    def __init__(self, samples_loader: BaseSamplesLoader):
        self._samples_loader = samples_loader

    def load_from_dir(self, directory: str, file_name_pattern: Pattern) -> Dict[int, List[List[int]]]:
        """
        Replaces file names produced by SamplesLoader as keys of a resulting dictionary with an enumeration
        :param directory: path to the directory containing files, where each file has events of one category
        :param file_name_pattern: pattern, with which file's name should comply in order to be processed
        :return: dictionary, where the key is a number and value is a list of lists containing measured values
        """
        events_by_file_name = self._samples_loader.load_from_dir(directory, file_name_pattern)
        remapped = {}
        i = 0
        for file_name, events in events_by_file_name.items():
            logging.debug('Enumeration: %s is now %d', file_name, i)
            remapped[i] = events
            i += 1
        remapped = collections.OrderedDict(remapped)
        return remapped

    @property
    def max_encountered_value(self) -> int:
        return self._samples_loader.max_encountered_value

    @property
    def longest_encountered_sequence(self) -> int:
        return self._samples_loader.longest_encountered_sequence

    def load_single_file(self, csv_file):
        return self._samples_loader.load_single_file(csv_file)
