import logging
import os
import sys

from predictor import *

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

task_class = Services.task_class()  # type: TaskClass
classification_report, confusion_matrix = task_class.load().run()
print(classification_report)
print(confusion_matrix)
