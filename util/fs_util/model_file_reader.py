from abc import ABC, abstractmethod
from typing import Callable

import tensorflow as tf

from tensorflow.python.keras.models import Model

from util.fs_util.functions import is_google_cloud_storage_address


class BaseModelFileReader(ABC):
    _load_model_callback = ...  # type: Callable[[str], Model]
    _file_name = ...  # type: str

    def __init__(self, file_name: str, load_model_callback: Callable):
        self._load_model_callback = load_model_callback
        self._file_name = file_name

    @abstractmethod
    def read(self) -> Model:
        pass

    @staticmethod
    def get_instance_by_file_name(file_name: str) -> 'BaseModelFileReader':
        callback = tf.keras.models.load_model \
            if file_name.endswith('.hdf5') \
            else tf.contrib.saved_model.load_keras_model

        if is_google_cloud_storage_address(file_name):
            raise NotImplementedError
        else:
            return LocalModelFileReader(file_name, callback)


class LocalModelFileReader(BaseModelFileReader):

    def read(self) -> Model:
        return self._load_model_callback(self._file_name)

