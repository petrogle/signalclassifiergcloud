from enum import Enum


class ModelExportFormatEnum(Enum):
    hdf5 = 0
    saved_model = 1
