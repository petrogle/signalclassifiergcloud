import os
from typing import List, Tuple

import numpy as np
import psutil
from google.cloud.storage import Client, Bucket, Blob

from trainer.constants import GOOGLE_CLOUD_STORAGE_PREFIX, GOOGLE_CREDENTIALS_PATH


def load_samples_from_file(file: str) -> List[int]:
    real_samples = np.loadtxt(file).tolist()  # type: List[float]
    return [int(real_sample) for real_sample in real_samples]


def parse_google_cloud_storage_address(address: str) -> Tuple[str, str]:
    if not address.startswith(GOOGLE_CLOUD_STORAGE_PREFIX):
        raise ValueError
    address_parts = tuple(address[len(GOOGLE_CLOUD_STORAGE_PREFIX):].split('/', 1))  # type: Tuple[str, str]
    return address_parts


def is_google_cloud_storage_address(address: str) -> bool:
    try:
        parse_google_cloud_storage_address(address)
        return True
    except ValueError:
        return False


def get_google_cloud_storage_client() -> Client:
    if get_google_cloud_storage_client.client is None:
        try:
            get_google_cloud_storage_client.client = Client.from_service_account_json(GOOGLE_CREDENTIALS_PATH)
        except FileNotFoundError:
            get_google_cloud_storage_client.client = Client()
    return get_google_cloud_storage_client.client


def get_google_storage_fqn(bucket: Bucket, blob: Blob, delimiter: str = '/'):
    return GOOGLE_CLOUD_STORAGE_PREFIX + bucket.name + delimiter + blob.name


def get_process_memory_usage() -> int:
    process = psutil.Process(os.getpid())
    return process.memory_info().rss
