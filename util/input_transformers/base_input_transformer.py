from abc import ABC, abstractmethod
from typing import Dict, Any, Sequence


class BaseInputTransformer(ABC):

    @abstractmethod
    def transform(self, seqs_of_seqs_by_class_id: Dict[int, Sequence[Sequence[Any]]]) -> Dict[int, Sequence[Sequence[Any]]]:
        pass

    @abstractmethod
    def transform_single_sequence(self, sequence: Sequence[Any]) -> Sequence[Any]:
        pass
