import logging
from typing import Sequence, Any

from scipy import signal

from util.input_transformers.base_single_sequence_based_transformer import BaseSingleSequenceBasedTransformer


class DownsamplingInputTransformer(BaseSingleSequenceBasedTransformer):
    FILTER_ORDER = 3  # type: int

    _downsampling_factor = ...  # type: int

    def __init__(self, downsampling_factor: int = 1):
        logging.debug('Downsampling factor is set to %d', downsampling_factor)
        self._downsampling_factor = downsampling_factor

    def transform_single_sequence(self, sequence: Sequence[Any]) -> Sequence[Any]:
        if self._downsampling_factor == 1:
            return sequence
        downsampled_samples = signal.decimate(sequence, self._downsampling_factor, self.FILTER_ORDER)
        min_value = min(min(downsampled_samples), 0)
        return [value - min_value for value in downsampled_samples]
