import logging
import re
from argparse import Namespace
from typing import List

from sklearn import metrics

from predictor.base_predictor import BasePredictor
from predictor.signal_classifier_predictor import SignalClassifierPredictor
from util import format_classification_report
from util.argument_parser import BaseArgumentParser
from util.fs_util.samples_loader import SamplesLoader
from util.input_transformers.input_transformer_factory import InputTransformerFactory


class TaskClass:
    CLASSIFICATION_REPORT_FILE = '/classification_report.txt'
    SAMPLES_FILE_PATTERN = '.+.csv$'

    _argument_parser = ...  # type: BaseArgumentParser
    _samples_loader = ...  # type: SamplesLoader
    _args = ...  # type: Namespace
    _predictor = ...  # type: BasePredictor

    _classes = ...  # type: List[str]
    _instances = ...  # type: List[List[int]]

    def __init__(self, argument_parser: BaseArgumentParser, samples_loader: SamplesLoader):
        self._samples_loader = samples_loader
        self._argument_parser = argument_parser
        self._args = self._argument_parser.parse_args()

    def load(self) -> 'TaskClass':
        logging.info('Loading samples...')
        input_by_e_id = self._samples_loader.load_from_dir(self._args.job_dir, re.compile(self.SAMPLES_FILE_PATTERN))
        logging.info('%d samples loaded', sum(len(inputs) for e_id, inputs in input_by_e_id.items()))

        logging.info('Applying preliminary transformations...')
        for transformer in InputTransformerFactory(class_mapping=self._args.map).get_transformers_chain():
            input_by_e_id = transformer.transform(input_by_e_id)

        logging.info('Preparing test instances...')
        self._classes, self._instances = [
            list(tpl) for tpl in zip(*[(e_id, item) for e_id, items in input_by_e_id.items() for item in items])
        ]

        logging.info('Instantiating predictor...')
        self._predictor = SignalClassifierPredictor.from_path(self._args.model_path)

        return self

    def run(self):
        output = self._predictor.predict(self._instances, **self._args.__dict__)

        classification_report = format_classification_report(metrics.classification_report(self._classes, output))
        confusion_matrix = metrics.confusion_matrix(self._classes, output)

        self._log(classification_report)

        return classification_report, confusion_matrix

    def _log(self, classification_report):
        if self._args.log_dir is not None:
            report_file_name = self._args.log_dir + self.CLASSIFICATION_REPORT_FILE
            logging.info('Saving report to %s...', report_file_name)
            with open(report_file_name, 'w') as f:
                f.write(classification_report)


