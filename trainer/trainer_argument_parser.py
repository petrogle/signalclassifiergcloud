from util.argument_parser import BaseArgumentParser


class TrainerArgumentParser(BaseArgumentParser):

    def setup(self):
        super().setup()
        self.add_argument("--epochs", type=int, nargs="?", default=5, help="""Number of training epochs (if not
            provided, is set to 5 by default)""")
        self.add_argument("--max-possible-value", type=int, nargs="?", default=None, help="""Maximum encountered value,
            in case, when it is not detectable otherwise (e.g. when it is preferable to avoid decimation on test data
            and it is known, that they include a higher maximum value in a comparison to a training data set)""")
