from tensorflow.python.keras import Model

from trainer.model_builder.base_model_builder import BaseModelBuilder
from trainer.model_builder.cnn_model_builder import CnnModelBuilder
from trainer.model_builder.rnn_model_builder import RnnModelBuilder
from trainer.model_builder.dense_model_builder import DenseModelBuilder
from util.model_type_enum import ModelTypeEnum


class ModelFactory:
    classes_count = ...  # type: int
    sequence_length = ...  # type: int
    max_possible_value = ...  # type: int

    def get_model(self, model_type: ModelTypeEnum) -> Model:
        return self._get_model_builder(model_type).build()

    def set_params(self, classes_count: int, sequence_length: int = None,
                   max_possible_value: int = None) -> 'ModelFactory':
        self.classes_count = classes_count
        self.sequence_length = sequence_length
        self.max_possible_value = max_possible_value
        return self

    def _get_model_builder(self, model_type: ModelTypeEnum) -> BaseModelBuilder:
        if model_type == ModelTypeEnum.dense:
            return DenseModelBuilder(self.classes_count)
        elif model_type == ModelTypeEnum.cnn:
            return CnnModelBuilder(self.classes_count, self.sequence_length, self.max_possible_value)
        elif model_type == ModelTypeEnum.rnn:
            return RnnModelBuilder(self.classes_count, self.sequence_length, self.max_possible_value)
        else:
            raise NotImplementedError
