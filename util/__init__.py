import re
from typing import List


def format_classification_report(original: str) -> str:
    tabbed = re.sub(r' {2,}', "\t", original)
    return "\t" + "\n".join(filter(None, [line.strip() for line in tabbed.splitlines()]))


def to_indicator(id: int, total_count: int) -> List[int]:
    return [int(n == id) for n in range(0, total_count)]