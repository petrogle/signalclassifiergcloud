from dependency_injector import containers, providers

from predictor import TaskClass, PredictorArgumentParser
from util.common_services import common_services


class Services(containers.DeclarativeContainer):
    task_class = providers.Factory(TaskClass, PredictorArgumentParser(), common_services.samples_loader())
