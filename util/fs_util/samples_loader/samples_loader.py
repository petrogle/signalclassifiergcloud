import collections
import csv
import logging
import os
from typing import List, Dict, TextIO, Pattern

from util.fs_util.samples_loader.base_samples_loader import BaseSamplesLoader


class SamplesLoader(BaseSamplesLoader):
    DELIMITER = ','
    LINE_TERMINATOR = '\n'

    VALUE_COLUMN_NAME = 'rawValue'
    EVENT_ID_COLUMN_NAME = 'ID'

    _value_column_index = ...  # type: int
    _event_id_column_index = ...  # type: int

    _max_encountered_value = 0  # type: int
    _longest_encountered_sequence = 0  # type: int

    def load_from_dir(self, directory: str, file_name_pattern: Pattern) -> Dict[str, List[List[int]]]:
        if not os.path.isdir(directory):
            raise NotADirectoryError('Bad directory provided %s' % directory)
        values = {}  # type: Dict[str, List[List[int]]]
        for path, dir_names, file_names in os.walk(directory):
            for file_name in sorted(file_names):
                if file_name_pattern.match(file_name) is None:
                    continue
                logging.info('Processing %s...', file_name)
                full_file_name = path + '/' + file_name
                values[file_name] = self.load_single_file(open(full_file_name))
                logging.info('%d events loaded', len(values[file_name]))

        values = collections.OrderedDict(sorted(values.items()))
        return values

    def load_single_file(self, csv_file: TextIO) -> List[List[int]]:
        csv_reader = csv.reader(csv_file, delimiter=self.DELIMITER, skipinitialspace=True)
        self._setup_column_indices(next(csv_reader))
        list_of_lists_of_values = []  # type: List[List[int]]
        last_event_id = -1
        try:
            while True:
                row = next(csv_reader)
                event_id = row[self._event_id_column_index]
                if event_id != last_event_id:
                    list_of_lists_of_values.append([])
                    last_event_id = event_id
                value = int(float(row[self._value_column_index]))
                list_of_lists_of_values[-1].append(value)
                self._max_encountered_value = max(self._max_encountered_value, value)
                self._longest_encountered_sequence = max(self._longest_encountered_sequence, len(list_of_lists_of_values[-1]))
        except StopIteration:
            pass
        return list_of_lists_of_values

    def save_dir(self, values: Dict[str, List[List[int]]], directory: str) -> None:
        """
        :param values: dictionary, where the key is an file name and value is a list of lists containing measured values
        :param directory: path to the directory, where one file per category should be saved
        """
        i = 0
        for file_name, list_of_lists_of_values in values.items():
            logging.info('Processing %d/%d category...', i, len(values.keys()))
            file_path = directory + '/' + file_name
            with open(file_path, 'w') as csv_file:
                self.save_single_file(list_of_lists_of_values, csv_file)
                logging.info('Saved to %s', file_path)
            i += 1

    def save_single_file(self, instances: List[List[int]], csv_file: TextIO) -> None:
        """
        :param instances: list of lists containing measured values
        :param csv_file: file open for writing
        """
        csv_writer = csv.writer(csv_file, delimiter=self.DELIMITER, lineterminator=self.LINE_TERMINATOR)
        csv_writer.writerow([self.EVENT_ID_COLUMN_NAME, self.VALUE_COLUMN_NAME])
        event_id = 0
        for list_of_values in instances:
            for value in list_of_values:
                csv_writer.writerow([event_id, float(value)])
            event_id += 1

    @property
    def max_encountered_value(self):
        return self._max_encountered_value

    @property
    def longest_encountered_sequence(self):
        return self._longest_encountered_sequence

    def _setup_column_indices(self, header: List[str]):
        self._value_column_index = header.index(self.VALUE_COLUMN_NAME)
        self._event_id_column_index = header.index(self.EVENT_ID_COLUMN_NAME)
