from setuptools import setup, find_packages

setup(
    name='SignalClassifierGCloud',
    version='',
    packages=find_packages(),
    url='',
    license='',
    author='Gleb Petrovichev',
    author_email='petrogle@fel.cvut.cz',
    description='',
    install_requires=['numpy', 'tensorflow', 'scipy', 'google-cloud-storage', 'scikit-learn', 'matplotlib', 'dependency-injector',
                      'psutil']
)
