from typing import Dict, List, Any, Sequence, Union

from util.input_transformers.base_input_transformer import BaseInputTransformer


class RemappingTransformer(BaseInputTransformer):
    _mapping = ...  # type: Dict[int, int]

    def __init__(self, mapping: Dict[int, int]):
        self._mapping = mapping

    def transform(self, seqs_of_seqs_by_class_id: Dict[int, Sequence[Sequence[Any]]]) -> Dict[int, Sequence[Sequence[Any]]]:
        if seqs_of_seqs_by_class_id.keys() != self._mapping.keys():
            raise ValueError('Keys in mapping are not equal to keys in dictionary')
        remapped_input_by_class_id = {}  # type: Dict[int, Union[List[Sequence[Any]], Sequence[Sequence[Any]]]]
        for old_class_id, seqs_of_seqs in seqs_of_seqs_by_class_id.items():
            new_class_id = self._mapping[old_class_id]
            if new_class_id in remapped_input_by_class_id:
                remapped_input_by_class_id[new_class_id].extend(seqs_of_seqs)
            else:
                remapped_input_by_class_id[new_class_id] = seqs_of_seqs
        return remapped_input_by_class_id

    def transform_single_sequence(self, sequence: Sequence[Any]) -> Sequence[Any]:
        return sequence
