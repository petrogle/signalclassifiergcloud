from tensorflow.keras import Model, Sequential, layers, optimizers

from trainer.model_builder import BaseModelBuilder


class RnnModelBuilder(BaseModelBuilder):
    EMBEDDING_VECTOR_SIZE = 10
    MIN_POSSIBLE_VALUE = 0
    RECURRENCE_BATCH = 3
    OUTPUT_LAYER_ACTIVATION = 'softmax'

    _max_possible_value = ...  # type: int
    _sequence_length = ...  # type: int

    def __init__(self, classes_count: int, sequence_length: int, max_possible_value: int):
        super().__init__(classes_count)
        self._sequence_length = sequence_length
        self._max_possible_value = max_possible_value

    def _do_build(self) -> Model:
        model = Sequential([
            layers.Embedding(self._max_possible_value - self.MIN_POSSIBLE_VALUE + 1, self.EMBEDDING_VECTOR_SIZE),
            layers.SimpleRNN(self.RECURRENCE_BATCH),
            layers.Dense(self.classes_count, self.OUTPUT_LAYER_ACTIVATION),
        ])

        return model
